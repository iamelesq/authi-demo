const passport = require('passport');

module.exports = (app) => {
    // authentication route that triggers the
    // passport / google oauth flow
    app.get(
        '/auth/google',
        passport.authenticate('google', { scope: ['profile', 'email'] })
    );

    // route that the oauth flow redirects to. It allows
    // passport to process it and on return runs a function
    // to redirect to / surveys
    app.get(
        '/auth/google/callback',
        passport.authenticate('google'),
        (req, res) => {
            res.redirect('/surveys');
        }
    );

    // logout facility and redirects to the landing
    app.get('/api/logout', (req, res) => {
        req.logout();
        res.redirect('/');
    });

    // route that returns the user object as retrieved
    // from the mongoDB call to findById.
    app.get('/api/currentuser', (req, res) => {
        res.send(req.user);
    });
};
