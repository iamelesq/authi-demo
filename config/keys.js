// creds controller

if (process.env.NODE_ENV === 'production') {
    // in prod
    module.exports = require('./prod');
} else {
    // dev keys
    module.exports = require('./dev');
}
