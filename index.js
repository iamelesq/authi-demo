const express = require('express');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');

const keys = require('./config/keys');

require('./models/user'); // order of operations requires the model before passport
require('./services/passport');

//mongoose.Promise = global.Promise;
mongoose
    .connect(keys.mongoUri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        dbName: keys.mongoDBName,
        user: keys.mongoUser,
        pass: keys.mongoPassword,
    })
    .then(() => {
        console.log('successfully connected to the database');
    })
    .catch((err) => {
        console.log('error connecting to the database', err);
        process.exit();
    });

const app = express();

app.use(express.json());

app.use(
    cookieSession({
        maxAge: 30 * 24 * 60 * 60 * 1000,
        keys: [keys.cookieKey],
    })
);

app.use(passport.initialize());
app.use(passport.session());

require('./routes/authRoutes')(app);
require('./routes/billingRoutes')(app);

if (process.env.NODE_ENV === 'production') {
    // ensure express serves up prod assets
    app.use(express.static('client/build'));

    // express serves up index.htm file on unrecognised route
    const path = require('path');
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    });
}

const PORT = process.env.PORT || 5000;
app.listen(PORT);
