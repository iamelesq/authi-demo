// development proxy setup to accommodate the react app being housed
// insode the server project. The react app will use relative routes
// throughout therefore a proxy to fire them at the correct address
// for the running server is required. (affects dev only)

const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = function (app) {
    app.use(
        ['/api', '/auth/google'],
        createProxyMiddleware({
            target: 'http://localhost:5000',
        })
    );
};
