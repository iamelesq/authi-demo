import React from 'react';
import StripeCheckout from 'react-stripe-checkout';
import { connect } from 'react-redux';
import * as actions from '../../actions';

class Payments extends React.Component {
    render() {
        return (
            <StripeCheckout
                name="Enki"
                description="$5 for 5 survey campiagn credits"
                amount={500}
                token={(token) => this.props.handleToken(token)}
                stripeKey={process.env.REACT_APP_STRIPE_PUBLISHABLE_KEY}
            >
                <button
                    className="btn btn-flat  grey darken-2"
                    style={{ color: 'white', borderRadius: '30px' }}
                >
                    Add Credits
                </button>
            </StripeCheckout>
        );
    }
}

export default connect(null, actions)(Payments);
