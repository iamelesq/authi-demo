import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../actions';

import Header from '../header/header.component';
import Landing from '../landing/landing.component';

import './app.styles.scss';

const SuccessScreen = () => (
    <div
        className="container"
        style={{
            textAlign: 'center',
        }}
    >
        <h3>You were Authenticated</h3>
    </div>
);

class App extends React.Component {
    componentDidMount() {
        this.props.fetchUser();
    }

    render() {
        return (
            <div className="app">
                <BrowserRouter>
                    <div>
                        <Header />
                        <Route exact path="/" component={Landing} />
                        <Route
                            exact
                            path="/surveys"
                            component={SuccessScreen}
                        />
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}

export default connect(null, actions)(App);
