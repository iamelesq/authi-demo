import React from 'react';

const Landing = () => {
    return (
        <div
            className="container"
            style={{
                textAlign: 'center',
            }}
        >
            <h1>Authi</h1>
            <p>Authi is a social login and authentication demo using Google.</p>
            <p>
                It handles a request and callback oauth flow for user
                authentication.
            </p>
        </div>
    );
};

export default Landing;
